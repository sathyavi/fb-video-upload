//
//  ViewController.h
//  FB
//
//  Created by Daniel Phillips on 06/10/2012.
//  Copyright (c) 2012 Daniel Phillips. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>

@interface ViewController : UIViewController

- (IBAction)getMe:(id)sender;
- (IBAction)uploadVideo:(id)sender;

@end
